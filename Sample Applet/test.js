let FAClient = null;
let recordId = null;
let values = {
  question_field0: "6eb47013-fcb2-48ff-990a-3de0833fb850",
  question_field1: "6eb47013-fcb2-48ff-990a-3de0833fb850",
  question_field2: "6eb47013-fcb2-48ff-990a-3de0833fb850",
  question_field4: "6eb47013-fcb2-48ff-990a-3de0833fb850"
};
const SERVICE = {
  name: 'FreeAgentService',
  appletId: `aHR0cHM6Ly9kOGFkYTNlNDExN2Mubmdyb2suaW8v`,
};

function startupService() {

  console.log("Excecuted");

  FAClient = new FAAppletClient({
    appletId: SERVICE.appletId,
  });

  FAClient.on("openForm", (data) => {
    console.log(data);
    getForm(data);
  });
  
}

function getForm(data) {
  console.log(data);
  recordId = data.field_values.contact_field73.value;
  removeTextMessage();
}

function removeTextMessage() {
  const loadingText = document.getElementById('loading-text');
  loadingText.remove();
  document.getElementById("hide").removeAttribute("hidden");
}
function enableCheck(e) {
  let check = {
    "true" : "490839cf-ab63-470e-a5f8-6a9b1304f2d8",
    "false" : "6eb47013-fcb2-48ff-990a-3de0833fb850"
  };
  let element = document.getElementById(e.id);
  values[element.value] = check[element.checked];
}
function createRecord(e) {
  try{
    let value = {
      entity : "question",
      field_values : {
        ...values
      },
      id : recordId
    }
    console.log(value);
    FAClient.updateEntity(value, (created) => {
      console.log(created);
    });

    return true;
  } catch(e){
    return true;
  }
}